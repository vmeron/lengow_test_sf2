<?php

namespace Vm\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Form;
use APY\DataGridBundle\Grid\Source\Entity;
use Vm\TestBundle\Entity\OrderData;

class OrderController extends Controller
{
    public function indexAction()
    {
        $source = new Entity('TestBundle:OrderData');
        $grid = $this->get('grid');
        $grid->setSource($source);
        $grid->isReadyForRedirect();
        
        return $this->render('TestBundle:Orders:index.html.twig', array(
            'grid' => $grid
        ));
    }

    public function newAction(Request $request)
    {
        $entity = new OrderData();
        $form = $this->createFormBuilder($entity)
            ->add('orderId', 'text')
            ->add('orderAmount', 'number')
            ->add('marketplace', 'text')
            ->add('orderPurchaseDate', 'datetime')
            ->add('save', 'submit', array('label' => 'Ajouter une commande'))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $em = $this->get('doctrine')->getManager();

            $order = $form->getData();
            $em->persist($order);
            $em->flush();

            return $this->redirectToRoute('test_orders_success');
        }

        return $this->render('TestBundle:Orders:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function successAction()
    {
        return $this->render('TestBundle:Orders:success.html.twig');
    }
}
