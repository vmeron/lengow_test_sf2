<?php

namespace Vm\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{
    /**
     * Api response.
     * 
     * @param Request $request
     * @return Response
     */
    public function ordersAction(Request $request)
    {
        $orderId = $request->get('id_order');

        if($orderId)
        {
            $data = $this->getOrderById($orderId);
        }
        else
        {
            $data = $this->getAllOrders();
        }

        return $this->handleResponse($data, $request);
    }

    protected function getAllowedFormats()
    {
        return array('json', 'yml');
    }

    /**
     * Return allowed mime type for given format.
     *
     * @param string $format
     * @return string
     */
    protected function getMimeForFormat($format)
    {
        $mimes = array(
            'json' => 'application/json',
            'yml' => 'text/yml'
        );

        return $mimes[$format];
    }

    /**
     * Return serialized response from given data.
     *
     * @param mixed $data
     * @param Request $request
     * @return Response
     */
    protected function handleResponse($data, Request $request)
    {
        $format = $request->get('format', 'json');
        $allowedFormat = $this->getAllowedFormats();

        if(!in_array($format, $allowedFormat))
        {
            return new Response('', 500);
        }

        $serialized = $this->get('serializer')->serialize($data, $format);
        $mime = $this->getMimeForFormat($format);
        $response = new Response($serialized);
        $response->headers->set('Content-Type', $mime);

        return $response;
    }

    /**
     * Fetch all orders.
     *
     * @return type
     */
    protected function getAllOrders()
    {
        $repo = $this->get('doctrine')
            ->getRepository('TestBundle:OrderData');
        return $repo->loadAll();
    }

    /**
     * Return one order from given id.
     *
     * @param int $orderId
     * @return OrderData
     */
    protected function getOrderById($orderId)
    {
        return $this->get('doctrine')
            ->getRepository('TestBundle:OrderData')
            ->find($orderId);
    }
}
