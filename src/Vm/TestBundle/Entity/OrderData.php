<?php

namespace Vm\TestBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderData
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Vm\TestBundle\Entity\OrderDataRepository")
 */
class OrderData
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="order_id", type="text")
     *
     * @Assert\NotBlank()
     */
    private $orderId;

    /**
     * @var float
     *
     * @ORM\Column(name="order_amount", type="float")
     * 
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="float"
     * )
     */
    private $orderAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="marketplace", type="string", length=255)
     * 
     * @Assert\NotBlank()
     */
    private $marketplace;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="order_purchase_date", type="datetime")
     *
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    private $orderPurchaseDate;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderId
     *
     * @param string $orderId
     * @return OrderData
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return string 
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set orderAmount
     *
     * @param float $orderAmount
     * @return OrderData
     */
    public function setOrderAmount($orderAmount)
    {
        $this->orderAmount = $orderAmount;

        return $this;
    }

    /**
     * Get orderAmount
     *
     * @return float 
     */
    public function getOrderAmount()
    {
        return $this->orderAmount;
    }

    /**
     * Set marketplace
     *
     * @param string $marketplace
     * @return OrderData
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;

        return $this;
    }

    /**
     * Get marketplace
     *
     * @return string 
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set orderPurchaseDate
     *
     * @param \DateTime $orderPurchaseDate
     * @return OrderData
     */
    public function setOrderPurchaseDate($orderPurchaseDate)
    {
        $this->orderPurchaseDate = $orderPurchaseDate;

        return $this;
    }

    /**
     * Get orderPurchaseDate
     *
     * @return \DateTime 
     */
    public function getOrderPurchaseDate()
    {
        return $this->orderPurchaseDate;
    }

    /**
     * Set orderPurchaseHeure
     *
     * @param \DateTime $orderPurchaseHeure
     * @return OrderData
     */
    public function setOrderPurchaseHeure($orderPurchaseHeure)
    {
        $this->orderPurchaseHeure = $orderPurchaseHeure;

        return $this;
    }
}
