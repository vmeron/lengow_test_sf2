<?php

namespace Vm\TestBundle\Parser;

interface ParserInterface
{
    public function getData();
}
