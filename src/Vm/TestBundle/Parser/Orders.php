<?php

namespace Vm\TestBundle\Parser;

use Symfony\Component\DependencyInjection\ContainerAware;
use Vm\TestBundle\Entity\OrderData;

class Orders extends ContainerAware
{
    protected $sourceUrl;
    protected $logger;
    protected $xmlData;

    public function __construct($sourceUrl, $logger)
    {
        $this->sourceUrl = $sourceUrl;
        $this->logger = $logger;
    }

    /**
     * Get xml data from stream or cached in instance.
     *
     * @return SimpleXmlElement
     */
    public function getData()
    {
        if(!$this->xmlData) {
            $this->read();
        }

        return $this->xmlData;
    }

    /**
     * Update all orders
     */
    public function update()
    {
        $doctrine = $this->container->get('doctrine');
        $manager = $doctrine->getManager();
        $repo = $doctrine->getRepository('TestBundle:OrderData');

        $orders = $this->getOrders();

        foreach($orders as $order) {
            $orderId = (string)$order->order_id;

            $existing = $repo->getByOrderId($orderId);

            if(is_null($existing)) {
                $this->saveNew($order);
            }
        }
        exit();
    }

    public function getOrders()
    {
        $data = $this->getData();
        $orders = $data->xPath('//order');
        
        return $orders;
    }

    /**
     * Make the call to the xml dump.
     * 
     * @return Vm\TestBundle\Parser\Orders
     */
    protected function read()
    {
        $this->logger->info('Reading orders data');
        $xmlString = file_get_contents($this->sourceUrl);
        $this->xmlData = simplexml_load_string($xmlString);
        
        return $this;
    }

    protected function saveNew(\SimpleXMLElement $order)
    {
        $db = $this->container->get('doctrine')->getManager();
        $datePurchased = new \DateTime($order->order_purchase_date.' '.$order->order_purchase_heure);

        $newOrder = new OrderData();
        $newOrder->setOrderId((string)$order->order_id);
        $newOrder->setOrderAmount((float)$order->order_amount);
        $newOrder->setMarketPlace((string)$order->marketplace);
        $newOrder->setOrderPurchaseDate($datePurchased);

        $db->persist($newOrder);
        $db->flush();
    }
}

