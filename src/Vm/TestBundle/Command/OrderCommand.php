<?php

namespace Vm\TestBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OrderCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this->setName('test:orders:update');
    }

    /**
     * Update orders from xml stream.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $orderParser = $container->get('test.order_parser');

        $output->writeln('Updating commands');
        $orderParser->update();
    }
}
